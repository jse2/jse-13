package ru.mirsaitov.tm.service;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SessionServiceTest {

    private final SessionService sessionService = new SessionService();

    private final Long userId = 1l;

    private final List<String> commands = new ArrayList<>();

    private final String COMMAND1 = "COMMAND1";

    private final String COMMAND2 = "COMMAND2";

    public SessionServiceTest() {
        commands.add(COMMAND1);
        commands.add(COMMAND2);
    }

    @Test
    public void shouldStartSession() {
        sessionService.closeSession();
        assertEquals(sessionService.getUserId(), null);
        sessionService.startSession(userId);
        assertEquals(sessionService.getUserId(), userId);
    }

    @Test
    public void shouldSaveCommand() {
        sessionService.closeSession();
        sessionService.startSession(userId);
        for (final String command : commands) {
            sessionService.addCommand(command);
        }

        assertTrue(commands.equals(sessionService.findAllCommand()));
    }

    @Test
    public void shouldClearSession() {
        sessionService.closeSession();
        sessionService.startSession(userId);
        for (final String command : commands) {
            sessionService.addCommand(command);
        }

        sessionService.closeSession();

        assertEquals(sessionService.getUserId(), null);
        assertEquals(sessionService.findAllCommand().size(), 0);
    }

    @Test
    public void shouldExistMaxHistoriSize() {
        sessionService.closeSession();
        sessionService.startSession(userId);

        int i = 0;
        for (; i < SessionService.HISTORY_SIZE; ++i) {
            sessionService.addCommand(((Integer)i).toString());
        }
        sessionService.addCommand(((Integer)i).toString());

        assertNotEquals(sessionService.findAllCommand().get(0), "0");
    }

}
