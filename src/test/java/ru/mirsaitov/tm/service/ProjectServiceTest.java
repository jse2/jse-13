package ru.mirsaitov.tm.service;

import org.junit.Test;
import ru.mirsaitov.tm.entity.Project;
import ru.mirsaitov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectServiceTest {

    private final long userId = 1l;

    private final String name = "name";

    private final String description = "description";

    private final ProjectService projectService = new ProjectService(new ProjectRepository());

    @Test
    public void shouldCreate() {
        Project project = projectService.create(name, userId);
        assertEquals(name, project.getName());
        assertEquals(name, project.getDescription());

        project = projectService.create(name, description, userId);
        assertEquals(name, project.getName());
        assertEquals(description, project.getDescription());
    }

    @Test
    public void shouldClear() {
        Project project = projectService.create(name, userId);
        assertTrue(!projectService.findAll(userId).isEmpty());

        projectService.clear(userId);
        assertTrue(projectService.findAll(userId).isEmpty());
    }

    @Test
    public void shouldFindAll() {
        List<Project> projects = new ArrayList<>();

        projects.add(projectService.create(name, userId));
        assertTrue(projects.equals(projectService.findAll(userId)));

        projects.add(projectService.create(name, description, userId));
        assertTrue(projects.equals(projectService.findAll(userId)));
    }

    @Test
    public void shouldFind() {
        Project project = projectService.create(name, description, userId);

        assertEquals(projectService.findById(project.getId(), userId), project);
        assertEquals(projectService.findByName(project.getName(), userId), project);
        assertEquals(projectService.findByIndex(0, userId), project);

        assertEquals(projectService.findById(1L, userId), null);
        assertEquals(projectService.findByName("1", userId), null);
        assertEquals(projectService.findByIndex(1, userId), null);
    }

    @Test
    public void shouldRemove() {
        Project project = projectService.create(name, description, userId);
        assertEquals(projectService.removeById(project.getId(), userId), project);

        project = projectService.create(name, description, userId);
        assertEquals(projectService.removeByName(project.getName(), userId), project);

        project = projectService.create(name, description, userId);
        assertEquals(projectService.removeByIndex(0, userId), project);

        assertEquals(projectService.removeById(1L, userId), null);
        assertEquals(projectService.removeByName("1", userId), null);
        assertEquals(projectService.removeByIndex(1, userId), null);
    }

    @Test
    public void shouldUpdate() {
        final String updateName = "9";
        final String updateDescription = "10";
        Project project = projectService.create(name, description, userId);

        assertEquals(projectService.updateByIndex(0, updateName, userId), project);
        assertEquals(project.getName(), updateName);

        assertEquals(projectService.updateById(project.getId(), updateDescription, updateName, userId), project);
        assertEquals(project.getName(), updateDescription);
        assertEquals(project.getDescription(), updateName);
    }

    @Test
    public void shouldSeparationData() {
        final Long anotherUser = 2l;
        Project project = projectService.create(name, description, userId);

        assertEquals(projectService.findByIndex(0, anotherUser), null);
        assertEquals(projectService.findAll(anotherUser).size(), 0);

        projectService.clear(anotherUser);

        assertEquals(projectService.findByIndex(0, userId), project);
        assertEquals(projectService.findAll(userId).size(), 1);
    }

}